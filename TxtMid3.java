import java.util.*;
import javax.sound.midi.*;
import java.io.*;
public class TxtMid3{
	public static PrintStream ps=null;
	public static MetaMessage compas(int numerador,int potencia,int cc,int bb){
		byte[]datos=new byte[4];
		MetaMessage m=new MetaMessage();
		datos[0]=(byte)numerador;
		//datos[1]=(byte)Math.round(Math.pow(2,potencia));
		datos[1]=(byte)potencia;
		datos[2]=(byte)cc;
		datos[3]=(byte)bb;
		try{
			m.setMessage(88,datos,datos.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return m;
	}
	public static MetaMessage tonalidad(int alteraciones,int modo){ //0 Mayor, 1 menor
		byte[]datos=new byte[2];
		MetaMessage m=new MetaMessage();
		datos[0]=(byte)alteraciones;
		datos[1]=(byte)modo;
		try{
			m.setMessage(59,datos,datos.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return m;
	}
	public static MetaMessage tempo(double bpm){
		return tempo(Math.round(60000000./bpm));
	}
	public static MetaMessage tempo(long v){
		byte[]datos=new byte[3];
		datos[2]=(byte)(v%256);
		v=(v-(0xFF&datos[2]));
		v=v/256;
		datos[1]=(byte)(v%256);
		v=(v-(0xFF&datos[1]));
		v=v/256;
		datos[0]=(byte)v;
		MetaMessage m2=new MetaMessage();
		try{
			m2.setMessage(81,datos,datos.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return m2;
	}
	public static MetaMessage nombre(String n){
		byte[]datos=n.getBytes();
		MetaMessage m=new MetaMessage();
		try{
			m.setMessage(0x03,datos,datos.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return m;
	}
	public static ShortMessage cambioPrograma(int c,int n){
		ShortMessage sm=new ShortMessage();
		try{
			sm.setMessage(0xC0+c,n,0);
		}catch(Exception e){
			e.printStackTrace();
		}
		return sm;
	}
	public static ShortMessage mensaje(int p1,int p2,int p3)throws Exception{
		ShortMessage sm=new ShortMessage();
		sm.setMessage(p1,p2,p3);
		return sm;
	}
	public static double imp(Sequence a,Sequence b,double cur){
		double r=cur;
		while(b.getTracks().length>a.getTracks().length){
			a.createTrack();
		}
		for(int i=0;i<b.getTracks().length;i++){
			for(int j=0;j<b.getTracks()[i].size();j++){
				MidiEvent e=b.getTracks()[i].get(j);
				double p=cur+a.getResolution()*e.getTick()/b.getResolution();
				a.getTracks()[i].add(new MidiEvent(e.getMessage(),Math.round(p)));
				if(r<p){
					r=p;
				}
			}
		}
		return r;
	}
	public static void main(String[]args){
		try{
			int nmn=6000;
			int nmx=0;
			String nombre="Pia07";
			int trans=0;
			Scanner s=new Scanner(new FileReader(nombre+".txt"));
			int res=384;
			Sequence sq=new Sequence(Sequence.PPQ,res,2);
			if(s.next().equals("RUTA")){
				sq=MidiSystem.getSequence(new File(s.next().replaceAll("\\?"," ")));
				s.close();
				s=new Scanner(new FileReader(nombre+".txt"));
				res=sq.getResolution();
				s.next();
				s.next();
				HashMap<Integer,Integer>canales=new HashMap<Integer,Integer>();
				for(int i=0;i<sq.getTracks().length;i++){
					for(int j=0;j<sq.getTracks()[i].size();j++){
						try{
							int canal=((ShortMessage)sq.getTracks()[i].get(j).getMessage()).getChannel();
							if(canales.get(canal)==null){
								System.out.println("Usa el canal:"+canal);
								canales.put(canal,1);
							}
						}catch(Exception ext){
						}
					}
				}
			}else{
				s.close();
				s=new Scanner(new FileReader(nombre+".txt"));
			}
			//res=res*3;
			double d=1;
			double n=1;
			double cur=0;
			boolean ini=false;
			boolean com=false;
			double pox=0;
			int curt=1;
			int curc=0;
			int velocidad=127;
			//sq.getTracks()[0].add(new MidiEvent(tempo(150.),0));
			//sq.getTracks()[0].add(new MidiEvent(tonalidad(-1,1),0));
			//sq.getTracks()[0].add(new MidiEvent(compas(3,2,96,8),0));
			//sq.getTracks()[0].add(new MidiEvent(compas(3,2,res,8),0));
			while(true){
				if(s.hasNextInt()){
					int nt=s.nextInt();
					if(com){
						continue;
					}
					ShortMessage sm=new ShortMessage();
					if(curc==9){
						sm.setMessage(0x90+curc,nt,velocidad);
					}else{
						/*if(nt+trans<30||nt+trans>70){
							System.exit(0);
						}*/
						sm.setMessage(0x90+curc,nt+trans,velocidad);
						if(nt+trans>nmx){
							nmx=nt+trans;
						}
						if(nt+trans<nmn){
							nmn=nt+trans;
						}
					}
					long pos=Math.round(cur);
/*if(ps==null){
	ps=new PrintStream(new FileOutputStream(new File("TxtMid3Log.txt")));
}*/
//ps.println(curc+":["+pos+"]["+pos/(3.5*res)+"]:["+pos%res+"]"+(pos%res)/(1.*res));
						MidiEvent me=new MidiEvent(sm,pos);
					sq.getTracks()[curt].add(me);
					sm=new ShortMessage();
					if(curc==9){
						sm.setMessage(0x80+curc,nt,0);
					}else{
						sm.setMessage(0x80+curc,nt+trans,0);
					}
					cur+=n*(1.*res)/d;
//ps.println(cur+":"+res*(cur/res)+":"+(cur-res*(cur/res))/(1.*res));
					pos=Math.round(cur);
					me=new MidiEvent(sm,pos);
					sq.getTracks()[curt].add(me);
					if(ini){
						cur=pox;
					}
				}else{
					if(!s.hasNext()){
						break;
					}
					String o=s.next();
					//System.out.println("o:"+o);
					if(o.equals("{")){
						com=true;
					}
					if(o.equals("}")){
						com=false;
						continue;
					}
					if(com){
						continue;
					}
					if(o.equals("t")){
						//System.out.println(s.next());
						//System.exit(0);
						double txx=s.nextDouble();
						sq.getTracks()[0].add(new MidiEvent(tempo(txx),Math.round(cur)));
					}
					if(o.equals("v")){
						velocidad=s.nextInt();
					}
					if(o.equals("x")){
						sq.getTracks()[0].add(new MidiEvent(tonalidad(s.nextInt(),s.nextInt()),Math.round(cur)));
					}
					if(o.equals("m")){
						System.out.println("Compás");
						sq.getTracks()[0].add(new MidiEvent(compas(s.nextInt(),s.nextInt(),24,8),Math.round(cur)));
					}
					if(o.equals("n")){
						sq.getTracks()[curt].add(new MidiEvent(nombre(s.next()),Math.round(cur)));
					}
					if(o.equals("o")){
						sq.getTracks()[curt].add(new MidiEvent(cambioPrograma(curc,s.nextInt()),Math.round(cur)));
					}
					if(o.equals("y")){
						trans=s.nextInt();
					}
					if(o.equals("r")){
						try{
							n=s.nextDouble();
							d=s.nextDouble();
						}catch(Exception e){
							try{
								n=s.nextInt();
								d=s.nextInt();
							}catch(Exception e2){
								System.out.println(s.next());
								break;
							}
						}
					}
					if(o.equals("s")){
						cur+=(int)Math.round(n*res/d);
					}
					if(o.equals("[")){
						ini=true;
						pox=cur;
					}
					if(o.equals("]")){
						ini=false;
						cur+=(int)Math.round(n*res/d);
					}
					if(o.equals("c")){
						sq.createTrack();
						cur=0;
						curt++;
						curc++;
					}
					if(o.equals("C")){
						cur=0;
						curc=s.nextInt();
					}
					if(o.equals("T")){
						cur=0;
						curt=s.nextInt();
					}
					if(o.equals("imp")){
						cur=imp(sq,MidiSystem.getSequence(new File(s.next().replaceAll("\\?"," "))),cur);
					}
				}
			}
			System.out.println("nmn:"+nmn);
			System.out.println("nmx:"+nmx);
			MidiSystem.write(sq,1,new File(nombre+".mid"));
			System.out.println(sq.getTickLength()/res);
			System.out.println(sq.getTickLength()/(4*res));
			MidiDevice.Info[]ifs=MidiSystem.getMidiDeviceInfo();
			MidiDevice midiDevice=MidiSystem.getMidiDevice(ifs[0]);
			midiDevice.open();
			Receiver receiver=midiDevice.getReceiver();
			Sequencer sx=MidiSystem.getSequencer(false);
			sx.getTransmitter().setReceiver(receiver);
			sx.setSequence(sq);
			sx.open();
			sx.start();
			System.in.read();
			sx.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}