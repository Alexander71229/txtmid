import javax.sound.midi.*;
import java.io.*;
import static c.U.imp;
public class Gatos04{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\[Free-scores.com]_victoria-tomas-luis-de-o-magnum-mysterium-452.midi"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\[Free-scores.com]_victoria-tomas-luis-de-o-magnum-mysterium-452.midi";
				MidiSystem.write(Util.transponer(a,-1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Anon-cor.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\Anon-cor.mid";
				MidiSystem.write(Util.modificarTempo(Util.transponer(a,-1),2),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Bien podéis corazón mío_Machado.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\Bien podéis corazón mío_Machado.mid";
				MidiSystem.write(Util.transponer(a,-1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Quarti.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\Quarti.mid";
				MidiSystem.write(Util.transponer(a,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\estasNochesAtanLargas_120.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\estasNochesAtanLargas_120.mid";
				MidiSystem.write(Util.transponer(a,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Lucinda tus cabellos_Juan de Torres-m.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\Lucinda tus cabellos_Juan de Torres-m.mid";
				MidiSystem.write(Util.transponer(a,-1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Escureçe_las_montañas_ManuelMachado.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\Escureçe_las_montañas_ManuelMachado.mid";
				MidiSystem.write(Util.transponer(a,-2),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\De los alamos vengo madre_JVasquez.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\De los alamos vengo madre_JVasquez.mid";
				MidiSystem.write(Util.modificarTempo(Util.transponer(a,-2),1.5),1,new File(destino));
			}
 			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}
