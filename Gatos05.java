import javax.sound.midi.*;
import java.io.*;
import static c.U.imp;
public class Gatos05{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\[Free-scores.com]_victoria-tomas-luis-de-o-magnum-mysterium-452.midi"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\acm\\[Free-scores.com]_victoria-tomas-luis-de-o-magnum-mysterium-452.midi";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,2,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\Anon-cor.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\acm\\Anon-cor.mid";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,0,2),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\Bien podéis corazón mío_Machado.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\acm\\Bien podéis corazón mío_Machado.mid";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,2,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\Quarti.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\acm\\Quarti.mid";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,2,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\estasNochesAtanLargas.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\acm\\estasNochesAtanLargas.mid";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,2,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\Lucinda tus cabellos_Juan de Torres-m.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\acm\\Lucinda tus cabellos_Juan de Torres-m.mid";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,2,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\Escureçe_las_montañas_ManuelMachado.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Mod\\acm\\Escureçe_las_montañas_ManuelMachado.mid";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,2,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\conQueLaLavare-Em.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm2\\conQueLaLavare-Em-acm.mid";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,2,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\conQueLaLavare-Em.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm2\\conQueLaLavare-Em-acm.mid";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,2,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Entre dos mansos arroyos_Capitán.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm2\\Entre dos mansos arroyos_Capitán.mid";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,2,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\De los alamos vengo madre_JVasquez.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm2\\De los alamos vengo madre_JVasquez.mid";
				MidiSystem.write(Util.simpleAcompanamientoCanal(a,2,2),1,new File(destino));
			}
 			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}
