import javax.sound.midi.*;
import java.io.*;
import java.util.*;
import static c.U.imp;
public class Carmen01{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
 			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Midi\\02-A1-2.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Midi\\T\\02-A1-2_T.mid";
				Util.modificarNota(a,Util.buscarEvento(a,4,0,5,0x90,55),56);
				MidiSystem.write(Util.obtenerCanal(Util.volumenFijo(a,127),4),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Midi\\02-A1-2.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Midi\\B\\02-A1-2_B";
				ArrayList<Sequence>l=Util.obtenerVoces(a,6);
				imp(l+"");
				for(int i=0;i<l.size();i++){
					MidiSystem.write(l.get(i),1,new File(destino+"_"+i+".mid"));
				}
			}
 			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Midi\\03-A1-3.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Midi\\S\\02-A1-2_S_1.mid";
				MidiSystem.write(Util.obtenerCanal(Util.volumenFijo(a,127),13),1,new File(destino));
			}
 			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Midi\\03-A1-3.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Midi\\S\\02-A1-2_S_2.mid";
				MidiSystem.write(Util.obtenerCanal(Util.volumenFijo(a,127),14),1,new File(destino));
			}
 			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}
