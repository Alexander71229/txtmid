function modificarCadena(cadena, valorRestar) {
    // Usamos una expresión regular para encontrar los números en la cadena
    return cadena.replace(/\d+/g, function(numero) {
        // Convertimos el número encontrado en entero
        let num = parseInt(numero);
        
        // Si el número es mayor a 10, restamos el valor especificado
        if (num > 10) {
            num += valorRestar;
        }
        
        // Retornamos el número modificado o el mismo número si es <= 10
        return num.toString();
    });
}

// Ejemplo de uso:
let cadena = `r 1 1 67 64 r 2 1 64 r 1 1 65 62 r 2 1 62 r 1 1 60 62 64 65 67 67 r 2 1 67
r 1 1 67 64 64 64 65 62 62 62 60 64 67 67 64 64 r 2 1 64
r 1 1 62 62 62 62 62 64 r 2 1 65 r 1 1 64 64 64 64 64 65 r 2 1 67
r 1 1 67 64 64 64 65 62 62 62 60 64 67 67 64 64 r 2 1 64
`;

let resultado = modificarCadena(cadena, 12);
console.log(resultado);
