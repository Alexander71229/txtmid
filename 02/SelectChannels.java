import java.io.*;
import javax.sound.midi.*;
public class SelectChannels{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			c.U.imp("Inicio");
			Sequence s=MidiSystem.getSequence(new File("C:\\Desarrollo\\Audio\\Coro\\AEDOS\\Midi\\PARRANDERAS120.mid"));
			Sequence r=new Sequence(s.getDivisionType(),s.getResolution(),2);
			for(int i=0;i<s.getTracks().length;i++){
				for(int j=0;j<s.getTracks()[i].size();j++){
					MidiEvent me=s.getTracks()[i].get(j);
					long tk=me.getTick();
					try{
						ShortMessage sm=(ShortMessage)me.getMessage();
						if(sm.getChannel()==4){
							r.getTracks()[1].add(me);
						}
					}catch(Exception e){
						r.getTracks()[0].add(me);
					}
				}
			}
			MidiSystem.write(r,1,new File("C:\\Desarrollo\\Audio\\Coro\\AEDOS\\Midi\\PARRANDERAS120Bajo.mid"));
			c.U.imp("Fin");
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}