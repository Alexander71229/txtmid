import javax.sound.midi.*;
import java.io.*;
import java.util.*;
import static c.U.imp;
public class Util{
	//Extraer solo las notas de la secuencia
	public static Sequence notas(Sequence s)throws Exception{
		Sequence r=new Sequence(s.getDivisionType(),s.getResolution(),s.getTracks().length);
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(me.getMessage().getStatus()>=0x90&&me.getMessage().getStatus()<=0x9F&&me.getMessage().getMessage().length>=2&&me.getMessage().getMessage()[1]>=0&&me.getMessage().getMessage()[1]<=128&&sm.getData2()>0){
						r.getTracks()[i].add(me);
					}
					if(((me.getMessage().getStatus()>=0x80&&me.getMessage().getStatus()<=0x8F)||(me.getMessage().getStatus()>=0x90&&me.getMessage().getStatus()<=0x9F&&sm.getData2()==0))&&me.getMessage().getMessage().length>=2&&me.getMessage().getMessage()[1]>=0&&me.getMessage().getMessage()[1]<=128){
						r.getTracks()[i].add(me);
					}
				}
			}
		}
		return r;
	}
	//Sincronizar a en b
	public static Sequence sincronizar(Sequence a,Sequence b)throws Exception{
		Sequence r=new Sequence(a.getDivisionType(),a.getResolution(),Math.min(16,a.getTracks().length+b.getTracks().length));
		MTempo mta=new MTempo(a);
		MTempo mtb=new MTempo(b);
		for(int i=0;i<a.getTracks().length;i++){
			Track t=a.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof MetaMessage){
					MetaMessage m=(MetaMessage)me.getMessage();
					if(m.getType()==81){
						continue;
					}
				}
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage m=(ShortMessage)me.getMessage();
					r.getTracks()[b.getTracks().length+i].add(new MidiEvent(new ShortMessage(m.getCommand(),b.getTracks().length+i,m.getData1(),m.getData2()),mtb.nanotk(mta.tiempo(me.getTick()))));
					continue;
				}
				r.getTracks()[b.getTracks().length+i].add(new MidiEvent(me.getMessage(),mtb.nanotk(mta.tiempo(me.getTick()))));
			}
		}
		for(int i=0;i<b.getTracks().length;i++){
			Track t=b.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage m=(ShortMessage)me.getMessage();
					r.getTracks()[i].add(new MidiEvent(new ShortMessage(m.getCommand(),i,m.getData1(),m.getData2()),mtb.nanotk(mta.tiempo(me.getTick()))));
					continue;
				}
				r.getTracks()[i].add(me);
			}
		}
		return r;
	}
	//Sincronizar lista de secuencias al primer elemento de la lista.
	public static Sequence sincronizar(List<Sequence>l)throws Exception{
		Sequence r=l.get(0);
		for(int i=1;i<l.size();i++){
			r=sincronizar(l.get(i),r);
		}
		return r;
	}
	//Tomar el track y canal de b y ponerlo en a.
	public static Sequence add(Sequence a,Sequence b,int at,int ac,int bt)throws Exception{
		for(int i=0;i<b.getTracks()[bt].size();i++){
			MidiEvent me=b.getTracks()[bt].get(i);
			if(me.getMessage()instanceof ShortMessage){
				ShortMessage sm=(ShortMessage)me.getMessage();
				a.getTracks()[at].add(new MidiEvent(new ShortMessage(sm.getCommand(),ac,sm.getData1(),sm.getData2()),me.getTick()));
			}
		}
		return a;
	}
	//Crear mensaje de tempo desde el bpm
	public static MetaMessage tempo(double bpm){
		return tempo(Math.round(60000000./bpm));
	}
	//Crear mensaje de tempo desde duración de un beat en nanosegundos
	public static MetaMessage tempo(long v){
		byte[]datos=new byte[3];
		datos[2]=(byte)(v%256);
		v=(v-(0xFF&datos[2]));
		v=v/256;
		datos[1]=(byte)(v%256);
		v=(v-(0xFF&datos[1]));
		v=v/256;
		datos[0]=(byte)v;
		MetaMessage m2=new MetaMessage();
		try{
			m2.setMessage(81,datos,datos.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return m2;
	}
	//Establecer la secuencia a un tempo fijo.
	public static Sequence tempoFijo(Sequence a,double tmp)throws Exception{
		Sequence r=new Sequence(a.getDivisionType(),a.getResolution(),a.getTracks().length);
		r.getTracks()[0].add(new MidiEvent(tempo(tmp),0));
		for(int i=0;i<a.getTracks().length;i++){
			Track t=a.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof MetaMessage){
					MetaMessage m=(MetaMessage)me.getMessage();
					if(m.getType()==81){
						continue;
					}
				}
				r.getTracks()[i].add(me);
			}
		}
		return r;
	}
	//Multiplicar el tempo por un factor
	public static Sequence modificarTempo(Sequence s,double f)throws Exception{
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof MetaMessage){
					MetaMessage mm=(MetaMessage)me.getMessage();
					if(mm.getType()==81){
						byte[]data=tempo(Math.round(Tempo.getTempo(me).val/f)).getData();
						mm.setMessage(81,data,data.length);
					}
				}
			}
		}
		return s;
	}
	//Establecer volumen fijo.
	public static Sequence volumenFijo(Sequence s,int v)throws Exception{
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(sm.getCommand()==0x90&&sm.getData2()>0){
						sm.setMessage(sm.getCommand(),sm.getChannel(),sm.getData1(),v);
					}
				}
			}
		}
		return s;
	}
	//Retornar una secuencia que tiene el mismo tiempo de la original, pero que no tiene cambios de tempo
	public static Sequence to120(Sequence s)throws Exception{
		Sequence r=new Sequence(s.getDivisionType(),s.getResolution(),s.getTracks().length);
		MTempo mts=new MTempo(s);
		MTempo mt1=new MTempo(s.getResolution());
		mt1.add(new Tempo(0,500000));
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof MetaMessage){
					MetaMessage m=(MetaMessage)me.getMessage();
					if(m.getType()==81){
						continue;
					}
				}
				r.getTracks()[i].add(new MidiEvent(me.getMessage(),mt1.nanotk(mts.tiempo(me.getTick()))));
			}
		}
		return r;
	}
	//Modificar el volumen de las notas en un factor.
	public static Sequence volumen(Sequence s,double f)throws Exception{
		Sequence r=new Sequence(s.getDivisionType(),s.getResolution(),s.getTracks().length);
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(me.getMessage().getStatus()>=0x90&&me.getMessage().getStatus()<=0x9F&&me.getMessage().getMessage().length>=2&&me.getMessage().getMessage()[1]>=0&&me.getMessage().getMessage()[1]<=128&&sm.getData2()>0){
						sm.setMessage(me.getMessage().getStatus(),sm.getData1(),(int)Math.round(f*sm.getData2()));
						r.getTracks()[i].add(new MidiEvent(sm,me.getTick()));
						continue;
					}
				}
				r.getTracks()[i].add(me);
			}
		}
		return r;
	}
	//Obtener la lista de acordes como un mapa cada intervalo fijo de beat (f*resolución)
	public static List<Map<Integer,Integer>>mapaAcordes(Sequence s,double f)throws Exception{
		int z=(int)Math.round(s.getResolution()*f);
		List<Map<Integer,Integer>>r=new ArrayList<>();
		r.add(new HashMap<>());
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			int p=0;
			long at=0;
			Map<Integer,Integer>h=new HashMap<>();
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if((sm.getStatus()>=0x90&&me.getMessage().getStatus()<=0x9F&&sm.getData2()>0)||((sm.getStatus()>=0x80&&sm.getStatus()<=0x8F)||(sm.getStatus()>=0x90&&sm.getStatus()<=0x9F&&sm.getData2()==0))){
						c.U.imp("e:"+p+"["+at+","+me.getTick()+"]"+":"+h);
						while(p*z<me.getTick()){
							if(at<=p*z){
								for(int k:h.keySet()){
									r.get(p).put(k,1);
									c.U.imp("x-1-"+i+"|"+p+":"+me.getTick()+":"+k);
								}
							}
							p++;
							c.U.imp("a:"+p);
							if(r.size()>=p){
								r.add(new HashMap<>());
							}
						}
						p--;
						if(sm.getStatus()>=0x90&&me.getMessage().getStatus()<=0x9F&&sm.getData2()>0){
							c.U.imp("p-"+me.getTick()+":"+sm.getData1());
							h.put(sm.getData1(),1);
							at=me.getTick();
						}
						if((sm.getStatus()>=0x80&&sm.getStatus()<=0x8F)||(sm.getStatus()>=0x90&&sm.getStatus()<=0x9F&&sm.getData2()==0)){
							c.U.imp("p-"+me.getTick()+":"+sm.getData1()+"<");
							h.remove(sm.getData1());
							at=me.getTick();
						}
					}
				}
			}
		}
		return r;
	}
	//Dado un acorde como un mapa, retornar la nota más alta que es menor que la nota dada como argumento
	public static int maxNota(Map<Integer,Integer>h,int ns){
		int r=-1;
		for(int z:h.keySet()){
			for(int i=0;(z%12)+12*i<ns;i++){
				if((z%12)+12*i>r){
					r=(z%12)+12*i;
				}
			}
		}
		return r;
	}
	//Dado un acorde como un mapa, retornar un acorde desde ns hacia abajo
	public static Map<Integer,Integer>maxAcorde(Map<Integer,Integer>h,int ns){
		Map<Integer,Integer>r=new HashMap<>();
		for(int z:h.keySet()){
			int n=-1;
			for(int i=0;(z%12)+12*i<ns;i++){
				n=(z%12)+12*i;
			}
			r.put(n,1);
		}
		return r;
	}
	//Obtener un mapa de los canales usados por los mensajes cortos
	public static Map<Integer,Integer>mapaCanales(Sequence s){
		Map<Integer,Integer>h=new HashMap<>();
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					h.put(sm.getChannel(),1);
				}
			}
		}
		return h;
	}
	//Mínimo canal melódico libre.
	public static int minimoCanalMelodico(Map<Integer,Integer>h){
		int r=-1;
		for(int i=0;i<16;i++){
			if(i!=9){
				if(h.get(i)==null){
					return i;
				}
			}
		}
		return r;
	}
	//Agregar una nota a una pista
	public static void agregarNota(Track t,int c,int n,int v,long tick,int l)throws Exception{
		t.add(new MidiEvent(new ShortMessage(0x90,c,n,v),tick));
		t.add(new MidiEvent(new ShortMessage(0x80,c,n,v),tick+l));
	}
	public static MidiEvent buscarEvento(Sequence s,int c,long k,int n,int a,int b)throws Exception{
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(me.getTick()>=k&&sm.getChannel()==c&&sm.getCommand()==a&&sm.getData1()==b){
						n--;
						if(n<=0){
							return me;
						}
					}
				}
			}
		}
		return null;
	}
	//Modificar nota
	public static Sequence modificarNota(Sequence s,MidiEvent e,int n)throws Exception{
		int x=-1;
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(me==e){
						x=sm.getData1();
						sm.setMessage(sm.getCommand(),sm.getChannel(),n,sm.getData2());
						continue;
					}
					if(x>0){
						sm.setMessage(sm.getCommand(),sm.getChannel(),n,sm.getData2());
						return s;
					}
				}
			}
		}
		return s;
	}
	//Simplificar la lista de acordes para evitar notas repetidas
	public static List<Map<Integer,Integer>>simplificarAcordes(List<Map<Integer,Integer>>acs){
		List<Map<Integer,Integer>>r=new ArrayList<>();
		for(int i=0;i<acs.size();i++){
			Map<Integer,Integer>h=new HashMap<>();
			for(int k:acs.get(i).keySet()){
				h.put(k%12,1);
			}
			r.add(h);
		}
		return r;
	}
	//Mapa de notas mínimas por Track
	public static Map<Integer,Integer>notasMinimas(Track t,int tick){
		Map<Integer,Integer>r=new HashMap<>();
		int mx=0;
		int p=0;
		for(int j=0;j<t.size();j++){
			MidiEvent me=t.get(j);
			if(me.getMessage()instanceof ShortMessage){
				ShortMessage sm=(ShortMessage)me.getMessage();
				if(sm.getChannel()!=9){
					if(sm.getCommand()==0x90||sm.getCommand()==0x80){
						if(p==0){
							p=sm.getData1();
						}
						int k=(int)me.getTick()/tick;
						if(k>mx){
							mx=k;
						}
						if(r.get(k)==null||r.get(k)>sm.getData1()){
							r.put(k,sm.getData1());
						}
					}
				}
			}
		}
		for(int i=0;i<mx;i++){
			if(r.get(i)==null){
				r.put(i,p);
			}
			p=r.get(i);
		}
		return r;
	}
	//Mapa de notas mínimas por Canal
	public static Map<Integer,Integer>notasMinimas(Sequence s,int cn,int tick){
		Map<Integer,Integer>r=new HashMap<>();
		int mx=0;
		int p=0;
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(sm.getChannel()==cn){
						if(sm.getCommand()==0x90||sm.getCommand()==0x80){
							if(p==0){
								p=sm.getData1();
							}
							int k=(int)me.getTick()/tick;
							if(k>mx){
								mx=k;
							}
							if(r.get(k)==null||r.get(k)>sm.getData1()){
								r.put(k,sm.getData1());
							}
						}
					}
				}
			}
		}
		for(int i=0;i<mx;i++){
			if(r.get(i)==null){
				r.put(i,p);
			}
			p=r.get(i);
		}
		return r;
	}
	//Crear un acompañamiento en notas individuales cada cierto factor de beat, colocando una nota justo debajo de la melodía indicada
	public static Sequence simpleAcompanamientoTrack(Sequence s,int tn,double f)throws Exception{
		Map<Integer,Integer>minimas=notasMinimas(s.getTracks()[tn],(int)Math.round(s.getResolution()*f));
		List<Map<Integer,Integer>>acs=simplificarAcordes(mapaAcordes(s,f));
		int c=minimoCanalMelodico(mapaCanales(s));
		Track t=s.createTrack();
		int an=-1;
		for(int i=0;i<acs.size();i++){
			if(acs.get(i).size()>0){
				if(acs.get(i).size()>1){
					acs.get(i).remove(an);
				}
				//imp("m-"+acs.get(i)+"-->"+an);
				int n=maxNota(acs.get(i),minimas.get(i));
				agregarNota(t,c,n,100,Math.round(s.getResolution()*i*f),(int)Math.round(s.getResolution()*f)-1);
				an=n%12;
			}
		}
		return s;
	}
	//Crear un acompañamiento en notas individuales cada cierto factor de beat, colocando una nota justo debajo de la melodía indicada
	public static Sequence simpleAcompanamientoCanal(Sequence s,int cn,double f)throws Exception{
		Map<Integer,Integer>minimas=notasMinimas(s,cn,(int)Math.round(s.getResolution()*f));
		List<Map<Integer,Integer>>acs=simplificarAcordes(mapaAcordes(s,f));
		int c=minimoCanalMelodico(mapaCanales(s));
		Track t=s.createTrack();
		int an=-1;
		for(int i=0;i<acs.size();i++){
			if(acs.get(i).size()>0){
				if(acs.get(i).size()>1){
					acs.get(i).remove(an);
				}
				//imp("m-"+acs.get(i)+"-->"+an);
				int n=maxNota(acs.get(i),minimas.get(i));
				agregarNota(t,c,n,100,Math.round(s.getResolution()*i*f),(int)Math.round(s.getResolution()*f)-1);
				an=n%12;
			}
		}
		return s;
	}
	//Crear un acompañamiento en notas individuales cada cierto factor de beat
	public static Sequence simpleAcompanamiento(Sequence s,int ns,double f)throws Exception{
		List<Map<Integer,Integer>>acs=simplificarAcordes(mapaAcordes(s,f));
		int c=minimoCanalMelodico(mapaCanales(s));
		Track t=s.createTrack();
		int an=-1;
		for(int i=0;i<acs.size();i++){
			if(acs.get(i).size()>0){
				if(acs.get(i).size()>1){
					acs.get(i).remove(an);
				}
				//imp("m-"+acs.get(i)+"-->"+an);
				int n=maxNota(acs.get(i),ns);
				agregarNota(t,c,n,100,Math.round(s.getResolution()*i*f),(int)Math.round(s.getResolution()*f)-1);
				an=n%12;
			}
		}
		return s;
	}
	//Crear un acompañamiento con acorde cada cierto factor de beat
	public static Sequence acompanamientoAcordes(Sequence s,int ns,double f)throws Exception{
		List<Map<Integer,Integer>>acs=simplificarAcordes(mapaAcordes(s,f));
		int c=minimoCanalMelodico(mapaCanales(s));
		Track t=s.createTrack();
		for(int i=0;i<acs.size();i++){
			if(acs.get(i).size()>0){
				//imp("m-"+acs.get(i)+"-->"+an);
				Map<Integer,Integer>a=maxAcorde(acs.get(i),ns);
				for(int n:a.keySet()){
					agregarNota(t,c,n,100,Math.round(s.getResolution()*i*f),(int)Math.round(s.getResolution()*f)-1);
				}
			}
		}
		return s;
	}
	//Obtener el canal i
	public static Sequence obtenerCanal(Sequence s,int c)throws Exception{
		Sequence r=new Sequence(s.getDivisionType(),s.getResolution(),2);
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(sm.getChannel()==c){
						r.getTracks()[0].add(me);
					}
				}else{
					r.getTracks()[0].add(me);
				}
			}
		}
		return r;
	}
	public static Sequence transponer(Sequence s,int v)throws Exception{
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(sm.getChannel()!=9){
						if(sm.getCommand()==0x90||sm.getCommand()==0x80){
							sm.setMessage(sm.getStatus(),sm.getData1()+v,sm.getData2());
						}
					}
				}
			}
		}
		return s;
	}
	//Lista de secuencias que representan las voces en un canal, separa la polifonía existente en secuencias.
	public static ArrayList<Sequence>obtenerVoces(Sequence s,int c)throws Exception{
		int mx=0;
		HashMap<MidiEvent,Integer>h=new HashMap<>();
		HashMap<MidiEvent,MidiEvent>u=new HashMap<>();
		HashMap<Integer,MidiEvent>o=new HashMap<>();
		ArrayList<MidiEvent>l=new ArrayList<>();
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(sm.getChannel()==c){
						if(sm.getCommand()==0x90&&sm.getData2()>0){
							o.put(sm.getData1(),me);
							h.put(me,sm.getData1());
							l.add(me);
							continue;
						}
						if((sm.getCommand()==0x90&&sm.getData2()==0)||sm.getCommand()==0x80){
							u.put(o.remove(sm.getData1()),me);
							h.put(me,sm.getData1());
							continue;
						}
					}
				}
			}
		}
		HashMap<MidiEvent,Integer>v=new HashMap<>();
		TreeMap<Integer,MidiEvent>z=new TreeMap<>();
		for(int i=0;i<l.size();i++){
			ArrayList<Integer>x=new ArrayList<>();
			for(Map.Entry<Integer,MidiEvent>e:z.entrySet()){
				if(l.get(i).getTick()>=u.get(e.getValue()).getTick()){
					x.add(e.getKey());
				}
			}
			if(z.size()>1&&x.size()>0){
				int n=1;
				for(Map.Entry<Integer,MidiEvent>e:z.entrySet()){
					if(v.get(e.getValue())==null){
						v.put(e.getValue(),n);
						v.put(u.get(e.getValue()),n);
						if(n>mx){
							mx=n;
						}
					}else{
						n=v.get(e.getValue());
					}
					n++;
				}
			}
			for(Integer y:x){
				z.remove(y);
			}
			z.put(h.get(l.get(i)),l.get(i));
		}
		ArrayList<Sequence>r=new ArrayList<>();
		for(int i=0;i<mx;i++){
			r.add(new Sequence(s.getDivisionType(),s.getResolution(),2));
		}
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(v.get(me)==null){
					for(int k=0;k<r.size();k++){
						if(me.getMessage()instanceof ShortMessage){
							ShortMessage sm=(ShortMessage)me.getMessage();
							if(sm.getChannel()==c){
								r.get(k).getTracks()[0].add(me);
							}
						}else{
							r.get(k).getTracks()[0].add(me);
						}
					}
				}else{
					r.get(mx-v.get(me)).getTracks()[0].add(me);
				}
			}
		}
		return r;
	}
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			//MidiSystem.write(sincronizar(MidiSystem.getSequence(new File("Dicen.mid")),MidiSystem.getSequence(new File("Dicen01.mid"))),1,new File("DicenSin01.mid"));
			//MidiSystem.write(to120(MidiSystem.getSequence(new File("DicenSin01.mid"))),1,new File("DicenSin01_120.mid"));
			/*for(int i=1;i<=8;i++){
				MidiSystem.write(to120(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Blanca\\Él\\Midi\\PARTE "+i+"-Coro.mid"))),1,new File("H:\\Desarrollo\\Audio\\Coro\\Blanca\\Él\\Midi\\120\\PARTE "+i+"-Coro_120.mid"));
			}*/
			/*for(int i=1;i<=8;i++){
				MidiSystem.write(volumen(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Blanca\\Él\\Midi\\120\\PARTE "+i+"-Coro_120.mid")),.25),1,new File("H:\\Desarrollo\\Audio\\Coro\\Blanca\\Él\\Midi\\120\\25\\PARTE "+i+"-Coro_120_25.mid"));
			}*/
			//MidiSystem.write(volumen(MidiSystem.getSequence(new File("Abre tu Corazón-back_120.mid")),.50),1,new File("Abre tu Corazón-back_120-50.mid"));
			//MidiSystem.write(to120(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Blanca\\Él\\Midi\\Completo\\MOV 1 CORO +ORQUESTA VERSIÓN MIDI.mid"))),1,new File("H:\\Desarrollo\\Audio\\Coro\\Blanca\\Él\\Midi\\Completo\\MOV 1 CORO +ORQUESTA VERSIÓN MIDI-120.mid"));
			//MidiSystem.write(tempoFijo(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\Andenken.mid")),70),1,new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\Andenken2.mid"));
			MidiSystem.write(to120(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\Andenken2.mid"))),1,new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\Andenken3.mid"));
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}