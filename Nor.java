import java.util.*;
import javax.sound.midi.*;
import java.io.*;
public class Nor{
	public static void main(String[]argumentos){
		try{
			String nom="TTBB KYRIE";
			Sequence s=MidiSystem.getSequence(new File(nom+".mid"));
			int res=s.getResolution();
			Track[]ts=s.getTracks();
			Sequence sq=new Sequence(Sequence.PPQ,res,ts.length);
			for(int i=0;i<ts.length;i++){
				Track t=ts[i];
				for(int j=0;j<t.size();j++){
					MidiEvent me=t.get(j);
					long tk=me.getTick();
					tk=Math.round(tk*1./res)*res;
					try{
						ShortMessage sm=(ShortMessage)me.getMessage();
						if((sm.getCommand()==ShortMessage.NOTE_OFF)||(sm.getCommand()==ShortMessage.NOTE_ON&&sm.getData2()==0)){
							tk--;
						}
					}catch(Exception e){
					}
					sq.getTracks()[i].add(new MidiEvent(me.getMessage(),tk));
				}
			}
			MidiSystem.write(sq,1,new File(nom+"_cuan.mid"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
