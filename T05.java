import javax.sound.midi.*;
import java.io.*;
import static c.U.imp;
public class T05{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\conQueLaLavare-Em.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\conQueLaLavare-Em-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\SiTeVas.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\SiTeVas.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\estasNochesAtanLargas.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\estasNochesAtanLargas.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Lucinda tus cabellos_Juan de Torres-m.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\Lucinda tus cabellos_Juan de Torres-m.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Anon-cor.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\Anon-cor.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Entre dos mansos arroyos_Capitán.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\Entre dos mansos arroyos_Capitán.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Bien podéis corazón mío_Machado.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\Bien podéis corazón mío_Machado.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Escureçe_las_montañas_ManuelMachado.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\Escureçe_las_montañas_ManuelMachado.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\[Free-scores.com]_victoria-tomas-luis-de-o-magnum-mysterium-452.midi"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\[Free-scores.com]_victoria-tomas-luis-de-o-magnum-mysterium-452.midi";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\De los alamos vengo madre_JVasquez.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\De los alamos vengo madre_JVasquez.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\Gaudent_in_coelis_Victoria.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\03\\acm\\Gaudent_in_coelis_Victoria.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}
