import java.io.*;
import java.util.Random;
public class Gen01{
	public static Random r;
	public static int g(int[]d){
		return d[r.nextInt(d.length)];
	}
	public static void main(String[]args){
		try{
			String nombre="Gen01x03";
			PrintStream ps=new PrintStream(new FileOutputStream(new File(nombre+".txt")));
			//int[]notas=new int[]{56,58,60,61,63,65,67,68,70,72,74,75,77,79,81,82};
			//int[]notas=new int[]{69,71,73,74,76,78,80,81};
			int[]notas=new int[]{55,57,59,60,62,64,66,67,69,71,73,74,76,78,80,81};
			int n=20;
			r=new Random();
			ps.println("t 80");
			ps.println("o 40");
			/*ps.println("r 1 2 69 71 73 74 76 78 80 81");
			ps.println("r 1 2 81 80 78 76 74 73 71 69");
			ps.println("r 1 2 69 71 73 74 76 78 80 81");
			ps.println("r 1 2 81 80 78 76 74 73 71 69");*/
			for(int i=0;i<n;i++){
				ps.println("r 1 2 "+g(notas)+" r 1 4 "+g(notas)+" "+g(notas)+" r 1 2 "+g(notas)+" "+g(notas)+" "+g(notas)+" "+g(notas)+" "+g(notas)+" "+g(notas));
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}