import javax.sound.midi.*;
import java.io.*;
import java.util.*;
import static c.U.imp;
public class T04{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			{
				List<Sequence>l=new ArrayList<>();
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\COMIENDO DEL MISMO PAN - Bajo.musx.mid")));
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\COMIENDO DEL MISMO PAN - Tenor.musx.mid")));
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\COMIENDO DEL MISMO PAN - Contralto.musx.mid")));
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\COMIENDO DEL MISMO PAN - Soprano.musx.mid")));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\COMIENDO DEL MISMO PAN.mid";
				MidiSystem.write(Util.to120(Util.sincronizar(l)),1,new File(destino));
			}
			{
				List<Sequence>l=new ArrayList<>();
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\SAGRADO CORAZÓN - Bajo.musx.mid")));
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\SAGRADO CORAZÓN - Contralto.musx.mid")));
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\SAGRADO CORAZÓN - Soprano.musx.mid")));
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\SAGRADO CORAZÓN - Tenor -  solista.musx.mid")));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\SAGRADO CORAZÓN.mid";
				MidiSystem.write(Util.to120(Util.sincronizar(l)),1,new File(destino));
			}
			{
				List<Sequence>l=new ArrayList<>();
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\TÚ REINARÁS - Bajo.musx.mid")));
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\TÚ REINARÁS - Contralto.musx.mid")));
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\TÚ REINARÁS - Soprano.musx.mid")));
				l.add(MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\TÚ REINARÁS - Tenor.musx.mid")));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Canticum Novum\\Midi\\TÚ REINARÁS.mid";
				MidiSystem.write(Util.to120(Util.sincronizar(l)),1,new File(destino));
			}
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}
