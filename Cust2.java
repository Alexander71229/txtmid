import java.util.*;
import javax.sound.midi.*;
import java.io.*;
public class Cust2{
	public static int id=0;
	public static int tx=0;
	public static double fx=1;
	public static Map<String,double[]>oto;
	public static String format(int id){
		String res=id+"";
		while(res.length()<4){
			res="0"+res;
		}
		return res;
	}
	public static void imprimir(PrintStream ps,long longitud,String letra,int nota,int intensidad,int tempo)throws Exception{
		ps.println("[#"+format(id)+"]");
		if(longitud<=0){
			System.out.println("Error en:"+letra);
			System.exit(0);
		}
		if(letra.equals("R")&&longitud>1920){
			longitud=1920;
		}
		ps.println("Length="+longitud);
		ps.println("Lyric="+letra);
		ps.println("NoteNum="+(nota+tx));
		ps.println("Intensity="+intensidad);
		ps.println("Modulation=0");
		if(tempo>0){
			System.out.println("Tempo="+tempo);
			ps.println("Tempo="+((int)(tempo*fx))+".00");
		}
		id++;
	}
	public static ArrayList<String>xlet(String letra)throws Exception{
		ArrayList<String>r=new ArrayList<String>();
		r=new ArrayList<>(Arrays.asList(letra.split("\\|")));
		ArrayList<String>r2=new ArrayList<String>();
		for(int i=0;i<r.size();i++){
			r2.add(r.get(i).replaceAll("&"," "));
		}
		return r2;
	}
	public static ArrayList<Long>llet(ArrayList<String>ls,long l,long tv,int cres)throws Exception{
		ArrayList<Long>r=new ArrayList<>();
		Map<String,Long>h=new HashMap<>();
		long tc=0;
		String x="";
		for(int i=0;i<ls.size();i++){
			if(!ls.get(i).startsWith(".")){
				try{
					h.put(ls.get(i),Math.round(oto.get(ls.get(i))[1]*120000/tv));
					tc+=h.get(ls.get(i));
				}catch(Throwable t){
					c.U.imp("Error:"+ls.get(i));
					c.U.imp(t);
				}
			}else{
				x=ls.get(i);
			}
		}
		if(tc>l){
			System.out.println("La duración de las constantes es superior a la duración de la nota");
			c.U.imp("La duración de las constantes es superior a la duración de la nota");
			System.exit(0);
		}
		h.put(x,l-tc);
		for(int i=0;i<ls.size();i++){
			r.add(h.get(ls.get(i)));
		}
		return r;
	}
	public static Map<String,double[]>cargarOto(File f)throws Exception{
		Map<String,double[]>r=new HashMap<>();
		Scanner s=new Scanner(f);
		while(s.hasNext()){
			String[]h=s.next().split("=")[1].split(",");
			double[]v=new double[5];
			r.put(h[0],v);
			for(int i=0;i<v.length;i++){
				v[i]=Double.parseDouble(h[i+1]);
			}
		}
		return r;
	}
	public static void main(String[]argumentos){
		try{
			c.U.ruta="logCust2.log";
			c.U.imp("Inicio");
			String nombre="C:\\Desarrollo\\Audio\\Coro\\Blanca\\Bienvenida\\midi\\BIENVENIDA NAVIDAD - PARTE 1-V0";
			String texto="Bienvenida";
			String vb="D:\\Java\\Txtmid\\txtmid\\VrSn\\banco\\04";
			//String vb="C:\\Desarrollo\\UST\\01\\Teren_VCCV_Spanish";
			int c=3;//Canal de la pista.
			tx=0; //12: tranporte.
			fx=1; //Factor del tempo.
			oto=cargarOto(new File(vb+"\\oto.ini"));
			Sequence sq=MidiSystem.getSequence(new File(nombre+".mid"));
			Scanner sc=new Scanner(new File(texto+"_c3.lyc"));
			PrintStream ps=new PrintStream(new FileOutputStream(new File(texto+".ust")));
			ps.println("[#VERSION]");
			ps.println("UST Version1.2");
			ps.println("[#SETTING]");
			ps.println("Tracks=1");
			ps.println("ProjectName="+nombre);
			//ps.println("VoiceDir=D:\\Tmp\\VrSn\\Banco\\Camila");
			//ps.println("VoiceDir=D:\\Tmp\\VrSn\\Banco\\01");
			//ps.println("VoiceDir=D:\\Java\\Txtmid\\txtmid\\VrSn\\banco\\01");
			ps.println("VoiceDir="+vb);
			//ps.println("VoiceDir=C:\\Desarrollo\\UST\\01\\Teren_VCCV_Spanish_Soft");
			ps.println("OutFile="+nombre);
			ps.println("CacheDir="+nombre+".cache");
			ps.println("Tool1=wavtool.exe");
			ps.println("Tool2=resampler.exe");
			ps.println("Mode2=True");
			int ores=sq.getResolution();
			int res=480;
			int tempo=120;
			int nota=0;
			int intensidad=100;
			int canal=-1;
			long longitud=0;
			boolean itempo=true;
			long db=500000;
			for(int i=0;i<sq.getTracks().length&&sc.hasNext();i++){
				for(int j=0;j<sq.getTracks()[i].size()&&sc.hasNext();j++){
					try{
						ShortMessage mensaje=((ShortMessage)sq.getTracks()[i].get(j).getMessage());
						if(mensaje.getChannel()==c){
							if(nota>0&&nota==mensaje.getData1()&&((mensaje.getCommand()==0x80)||(mensaje.getCommand()==0x90&&mensaje.getData2()==0))){
								longitud=((long)(sq.getTracks()[i].get(j).getTick()*(res*1./ores)))-longitud;
								String letra=sc.next();
								ArrayList<String>letras=xlet(letra);
								ArrayList<Long>lns=llet(letras,longitud,db,res);
								int xtempo=0;
								if(itempo){
									xtempo=tempo;
								}
								for(int k=0;k<letras.size();k++){
									imprimir(ps,lns.get(k),letras.get(k).replaceAll("\\.",""),nota,intensidad,xtempo);
								}
								itempo=false;
								longitud=((long)(sq.getTracks()[i].get(j).getTick()*(res*1./ores)));
								nota=0;
							}
							if(mensaje.getCommand()==0x90&&mensaje.getData2()>0&&nota==0){
								nota=mensaje.getData1();
								intensidad=mensaje.getData2();
								canal=mensaje.getChannel();
								if(((long)(sq.getTracks()[i].get(j).getTick()*(res*1./ores)))-longitud>0){
									int xtempo=0;
									if(itempo){
										xtempo=tempo;
									}
									imprimir(ps,((long)(sq.getTracks()[i].get(j).getTick()*(res*1./ores)))-longitud,"R",60,0,xtempo);
									itempo=false;
								}
								longitud=((long)(sq.getTracks()[i].get(j).getTick()*(res*1./ores)));
							}
						}
					}catch(Exception ext){
					}
					try{
						Tempo otempo=Tempo.getTempo(sq.getTracks()[i].get(j));
						if(otempo!=null){
							db=otempo.val;
							if(tempo!=(int)otempo.val){
								itempo=true;
							}
							tempo=(int)(60000000./otempo.val);
							System.out.println("Tempo="+tempo);
						}
					}catch(Exception ext){
					}
				}
			}
			imprimir(ps,480,"R",60,0,0);
			ps.println("[#TRACKEND]");
		}catch(Exception t){
			c.U.imp(t);
			t.printStackTrace();
		}
	}
}