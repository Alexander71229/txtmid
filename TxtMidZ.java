import java.util.*;
import javax.sound.midi.*;
import java.io.*;
public class TxtMidZ{
	public static MetaMessage compas(int numerador,int potencia,int cc,int bb){
		byte[]datos=new byte[4];
		MetaMessage m=new MetaMessage();
		datos[0]=(byte)numerador;
		//datos[1]=(byte)Math.round(Math.pow(2,potencia));
		datos[1]=(byte)potencia;
		datos[2]=(byte)cc;
		datos[3]=(byte)bb;
		try{
			m.setMessage(88,datos,datos.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return m;
	}
	public static MetaMessage tonalidad(int alteraciones,int modo){ //0 Mayor, 1 menor
		byte[]datos=new byte[2];
		MetaMessage m=new MetaMessage();
		datos[0]=(byte)alteraciones;
		datos[1]=(byte)modo;
		try{
			m.setMessage(59,datos,datos.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return m;
	}
	public static MetaMessage tempo(double bpm){
		return tempo(Math.round(60000000./bpm));
	}
	public static MetaMessage tempo(long v){
		byte[]datos=new byte[3];
		datos[2]=(byte)(v%256);
		v=(v-(0xFF&datos[2]));
		v=v/256;
		datos[1]=(byte)(v%256);
		v=(v-(0xFF&datos[1]));
		v=v/256;
		datos[0]=(byte)v;
		MetaMessage m2=new MetaMessage();
		try{
			m2.setMessage(81,datos,datos.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return m2;
	}
	public static MetaMessage nombre(String n){
		byte[]datos=n.getBytes();
		MetaMessage m=new MetaMessage();
		try{
			m.setMessage(0x03,datos,datos.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return m;
	}
	public static ShortMessage cambioPrograma(int c,int n){
		ShortMessage sm=new ShortMessage();
		try{
			sm.setMessage(0xC0+c,n,0);
		}catch(Exception e){
			e.printStackTrace();
		}
		return sm;
	}
	public static ShortMessage mensaje(int p1,int p2,int p3)throws Exception{
		ShortMessage sm=new ShortMessage();
		sm.setMessage(p1,p2,p3);
		return sm;
	}
	public static void main(String[]args){
		try{
			String nombre="Brisas Referencia";
			int trans=0;
			Scanner s=new Scanner(new FileReader(nombre+".txt"));
			int res=384;
			Sequence sq=new Sequence(Sequence.PPQ,res,2);
			//res=res*3;
			double d=1;
			double n=1;
			int cur=0;
			boolean ini=false;
			boolean com=false;
			int pox=0;
			int curt=1;
			int velocidad=127;
			//sq.getTracks()[0].add(new MidiEvent(tempo(150.),0));
			//sq.getTracks()[0].add(new MidiEvent(tonalidad(-1,1),0));
			//sq.getTracks()[0].add(new MidiEvent(compas(3,2,96,8),0));
			//sq.getTracks()[0].add(new MidiEvent(compas(3,2,res,8),0));
			while(true){
				if(s.hasNextInt()){
					int nt=s.nextInt();
					if(com){
						continue;
					}
//Temporal
					sq.getTracks()[1].add(new MidiEvent(mensaje(0x90,108,1),cur));
					sq.getTracks()[1].add(new MidiEvent(mensaje(0x80,108,1),cur+10));
//Fin Temporal					
					ShortMessage sm=new ShortMessage();
					if(curt==10){
						sm.setMessage(0x90+curt-1,nt,velocidad);
					}else{
						sm.setMessage(0x90+curt-1,nt+trans,velocidad);
					}
					long pos=cur;
					MidiEvent me=new MidiEvent(sm,pos+100);
					sq.getTracks()[curt].add(me);
					sm=new ShortMessage();
					if(curt==10){
						sm.setMessage(0x80+curt-1,nt,0);
					}else{
						sm.setMessage(0x80+curt-1,nt+trans,0);
					}
					cur+=(int)Math.round(n*res/d);
					pos=cur;
					me=new MidiEvent(sm,pos+100);
					sq.getTracks()[curt].add(me);
					if(ini){
						cur=pox;
					}
				}else{
					if(!s.hasNext()){
						break;
					}
					String o=s.next();
					if(o.equals("{")){
						com=true;
					}
					if(o.equals("}")){
						com=false;
						continue;
					}
					if(com){
						continue;
					}
					if(o.equals("t")){
						sq.getTracks()[0].add(new MidiEvent(tempo(s.nextDouble()),cur));
					}
					if(o.equals("v")){
						velocidad=s.nextInt();
					}
					if(o.equals("x")){
						sq.getTracks()[0].add(new MidiEvent(tonalidad(s.nextInt(),s.nextInt()),cur));
					}
					if(o.equals("m")){
						System.out.println("Comp�s");
						sq.getTracks()[0].add(new MidiEvent(compas(s.nextInt(),s.nextInt(),24,8),cur));
					}
					if(o.equals("n")){
						sq.getTracks()[curt].add(new MidiEvent(nombre(s.next()),cur));
					}
					if(o.equals("o")){
						sq.getTracks()[curt].add(new MidiEvent(cambioPrograma(curt-1,s.nextInt()),cur));
					}
					if(o.equals("r")){
						try{
							n=s.nextDouble();
							d=s.nextDouble();
						}catch(Exception e){
							try{
								n=s.nextInt();
								d=s.nextInt();
							}catch(Exception e2){
								System.out.println(s.next());
								break;
							}
						}
					}
					if(o.equals("s")){
						cur+=(int)Math.round(n*res/d);
					}
					if(o.equals("[")){
						ini=true;
						pox=cur;
					}
					if(o.equals("]")){
						ini=false;
						cur+=(int)Math.round(n*res/d);
					}
					if(o.equals("c")){
						sq.createTrack();
						cur=0;
						curt++;
					}
				}
			}
			MidiSystem.write(sq,1,new File(nombre+".mid"));
			MidiDevice.Info[]ifs=MidiSystem.getMidiDeviceInfo();
			MidiDevice midiDevice=MidiSystem.getMidiDevice(ifs[0]);
			midiDevice.open();
			Receiver receiver=midiDevice.getReceiver();
			Sequencer sx=MidiSystem.getSequencer(false);
			sx.getTransmitter().setReceiver(receiver);
			sx.setSequence(sq);
			sx.open();
			sx.start();
			System.in.read();
			sx.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}