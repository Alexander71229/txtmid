import java.util.*;
import javax.sound.midi.*;
import java.io.*;
public class Cust{
	public static int id=0;
	public static int tx=0;
	public static double fx=1;
	public static Map<String,String>hash;
	public static String format(int id){
		String res=id+"";
		while(res.length()<4){
			res="0"+res;
		}
		return res;
	}
	public static String reemplazar(String letra){
		for(String k:hash.keySet()){
			letra=letra.replace(k,hash.get(k));
		}
		return letra;
	}
	//imprimir(ps,longitud,letra,nota,intensidad,tempo)
	public static void imprimir(PrintStream ps,long longitud,String letra,int nota,int intensidad,int tempo)throws Exception{
		ps.println("[#"+format(id)+"]");
		if(longitud<=0){
			System.out.println("Error en:"+letra);
			System.exit(0);
		}
		if(letra.equals("R")&&longitud>1920){
			longitud=1920;
		}
		ps.println("Length="+longitud);
		ps.println("Lyric="+reemplazar(letra));
		ps.println("NoteNum="+(nota+tx));
		ps.println("Intensity="+intensidad);
		ps.println("Modulation=0");
		if(tempo>0){
			System.out.println("Tempo="+tempo);
			ps.println("Tempo="+((int)(tempo*fx))+".00");
		}
		id++;
	}
	public static boolean esV(String l){
		HashMap<String,Integer>h=new HashMap<String,Integer>();
		h.put("a",1);
		h.put("e",1);
		h.put("i",1);
		h.put("o",1);
		h.put("u",1);
		return h.get(l)!=null;
	}
	public static boolean esX(String s){
		return esV(s.substring(s.length()-1));
	}
	public static ArrayList<String>xlet(String letra)throws Exception{
		/*if(Math.sin(34234)<1000){
			ArrayList<String>r=new ArrayList<String>();
			r.add(letra);
			return r;
		}*/
		char[]x=letra.toCharArray();
		ArrayList<String>r=new ArrayList<String>();
		if(letra.indexOf("-")>0){
			r=new ArrayList(Arrays.asList(letra.split("-")));
			//System.out.println(letra+"->"+r);
			return r;
		}
		String p1="";
		boolean v=false;
		for(int i=0;i<x.length;i++){
			if(v){
				r.add(x[i]+"");
			}else{
				if(esV(x[i]+"")){
					p1=p1+x[i];
					r.add(p1);
					v=true;
				}
				p1=p1+x[i];
			}
		}
		//System.out.println(letra+"->"+r);
		return r;
	}
	public static void main(String[]argumentos){
		try{
			String nombre="Colombia";
			hash=new HashMap<>();
			hash.put("$","");
			hash.put("&"," ");
			hash.put("|","-");
			hash.put("%i","y");
			int cd=60; //Duraci�n de las consonantes finales.
			//char xv='a'; //vocal de las consonantes.
			char xv='$'; //vocal de las consonantes.
			int c=0;//Canal de la pista.
			tx=0; //12: tranporte.
			fx=1; //Factor del tempo.
			Sequence sq=MidiSystem.getSequence(new File(nombre+".mid"));
			Scanner sc=new Scanner(new File(nombre+"_t.lyc"));
			PrintStream ps=new PrintStream(new FileOutputStream(new File(nombre+".ust")));
			PrintStream log=new PrintStream(new FileOutputStream(new File(nombre+".log")));
			ps.println("[#VERSION]");
			ps.println("UST Version1.2");
			ps.println("[#SETTING]");
			ps.println("Tracks=1");
			ps.println("ProjectName="+nombre);
			//ps.println("VoiceDir=D:\\Tmp\\VrSn\\Banco\\Camila");
			//ps.println("VoiceDir=D:\\Tmp\\VrSn\\Banco\\01");
			//ps.println("VoiceDir=D:\\Java\\Txtmid\\txtmid\\VrSn\\banco\\01");
			ps.println("VoiceDir=D:\\Java\\Txtmid\\txtmid\\VrSn\\banco\\02");
			//ps.println("VoiceDir=C:\\Desarrollo\\UST\\01\\Teren_VCCV_Spanish_Soft");
			ps.println("OutFile="+nombre);
			ps.println("CacheDir="+nombre+".cache");
			ps.println("Tool1=wavtool.exe");
			ps.println("Tool2=resampler.exe");
			ps.println("Mode2=True");
			int ores=sq.getResolution();
			int res=480;
			int tempo=120;
			int nota=0;
			int intensidad=100;
			int canal=-1;
			long longitud=0;
			boolean itempo=true;
			for(int i=0;i<sq.getTracks().length;i++){
				for(int j=0;j<sq.getTracks()[i].size();j++){
					try{
						ShortMessage mensaje=((ShortMessage)sq.getTracks()[i].get(j).getMessage());
						if(mensaje.getChannel()==c){
							if(nota>0&&nota==mensaje.getData1()&&((mensaje.getCommand()==0x80)||(mensaje.getCommand()==0x90&&mensaje.getData2()==0))){
								if(!sc.hasNext()){
									sc.close();
									imprimir(ps,480,"R",60,0,0);
									System.exit(0);
								}
								longitud=((long)(sq.getTracks()[i].get(j).getTick()*(res*1./ores)))-longitud;
								String letra=sc.next();
								ArrayList<String>letras=xlet(letra);
								int xcd=cd;
								double ncd=(longitud*1.)/letra.length();
								//System.out.println(longitud+"-"+letras+":"+ncd);
								if(ncd<cd){
									xcd=((int)ncd);
								}
								int xtempo=0;
								if(itempo){
									xtempo=tempo;
								}
								int ix=0;
								for(int k=0;k<letras.size();k++){
									if(esX(letras.get(k))){
										ix=k;
									}
								}
								int cx=letra.replace("-","").length()-letras.get(ix).length();
								for(int k=0;k<letras.size();k++){
									if(k>0){
										xtempo=0;
									}
									if(k==ix){System.out.println(letra+":"+longitud+","+xcd+":"+cx+"-->"+letra.length()+","+letras.get(ix).length()+" nota:"+nota);
										imprimir(ps,longitud-cx*xcd,letras.get(k),nota,intensidad,xtempo);
									}else{
										if(esX(letras.get(k))){
											imprimir(ps,xcd*letras.get(k).length(),letras.get(k),nota,intensidad,xtempo);
										}else{
											imprimir(ps,xcd*letras.get(k).length(),letras.get(k)+xv,nota,intensidad,xtempo);
										}
									}
								}
								/*if(letras.size()==1){
									imprimir(ps,longitud,letras.get(0),nota,intensidad,xtempo);
								}
								if(letras.size()>=2){
									int lx=(letras.size()-2)*xcd;
									if(esV(letras.get(1))){
										imprimir(ps,2*xcd,letras.get(0),nota,intensidad,xtempo);
										imprimir(ps,longitud-2*xcd-lx,letras.get(1),nota,intensidad,0);
									}else{
										imprimir(ps,longitud-xcd-lx,letras.get(0),nota,intensidad,xtempo);
										imprimir(ps,xcd,letras.get(1)+xv,nota,intensidad,0);
									}
									for(int k=2;k<letras.size();k++){
										if(esV(letras.get(k))){
											imprimir(ps,xcd,letras.get(k),nota,intensidad,0);
										}else{
											imprimir(ps,xcd,letras.get(k)+xv,nota,intensidad,0);
										}
									}
								}*/
								itempo=false;
								longitud=((long)(sq.getTracks()[i].get(j).getTick()*(res*1./ores)));
								nota=0;
							}
							if(mensaje.getCommand()==0x90&&mensaje.getData2()>0&&nota==0){
								nota=mensaje.getData1();
								intensidad=mensaje.getData2();
								canal=mensaje.getChannel();
								if(((long)(sq.getTracks()[i].get(j).getTick()*(res*1./ores)))-longitud>0){
									int xtempo=0;
									if(itempo){
										xtempo=tempo;
									}
									imprimir(ps,((long)(sq.getTracks()[i].get(j).getTick()*(res*1./ores)))-longitud,"R",60,0,xtempo);
									itempo=false;
								}
								longitud=((long)(sq.getTracks()[i].get(j).getTick()*(res*1./ores)));
							}
						}
					}catch(Exception ext){
					}
					try{
						Tempo otempo=Tempo.getTempo(sq.getTracks()[i].get(j));
						if(otempo!=null){
							if(tempo!=(int)otempo.val){
								itempo=true;
							}
							tempo=(int)(60000000./otempo.val);
							System.out.println("Tempo="+tempo);
						}
					}catch(Exception ext){
					}
				}
			}
			imprimir(ps,480,"R",60,0,0);
			ps.println("[#TRACKEND]");
		}catch(Exception t){
			t.printStackTrace();
		}
	}
}