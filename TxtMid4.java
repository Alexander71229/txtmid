import java.util.*;
import javax.sound.midi.*;
import java.io.*;
public class TxtMid4{
	public static long f(long v){
		return Math.round(384.*v/22050.);
	}
	public static ShortMessage mensaje(int p1,int p2,int p3)throws Exception{
		ShortMessage sm=new ShortMessage();
		sm.setMessage(p1,p2,p3);
		return sm;
	}
	public static MidiEvent mx(long pos,int p1,int p2,int p3)throws Exception{
		return new MidiEvent(mensaje(p1,p2,p3),pos);
	}
	public static void nota(Sequence sq,long ini,int n,long fin)throws Exception{
		if(n>0){
			sq.getTracks()[1].add(mx(f(ini),0x90,n,90));
			sq.getTracks()[1].add(mx(f(fin),0x80,n,0));
		}
	}
	public static void main(String[]args){
		try{
			String nombre="Volver a amar";
			Scanner s=new Scanner(new FileReader(nombre+".tx4"));
			int res=384;
			Sequence sq=new Sequence(Sequence.PPQ,res,2);
			long ap=s.nextLong();
			int an=s.nextInt();
			while(s.hasNextLong()){
				long p=s.nextLong();
				nota(sq,ap,an,p);
				int n=s.nextInt();
				ap=p;
				an=n;
			}
			MidiSystem.write(sq,1,new File(nombre+".mid"));
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}
