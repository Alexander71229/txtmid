import java.io.*;
import javax.sound.midi.*;
public class CTempo{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			c.U.imp("Inicio");
			Sequence s=MidiSystem.getSequence(new File("C:\Desarrollo\Audio\Coro\AEDOS\Midi\PARRANDERAS.mid"));
			MTempo mt=new MTempo(s);
			int x=s.getResolution();
			Sequence r=new Sequence(s.getDivisionType(),x,s.getTracks().length);
			r.getTracks()[0].add(new MidiEvent(tempo(500000),0));
			for(int i=0;i<s.getTracks().length;i++){
				for(int j=0;j<s.getTracks()[i].size();j++){
					MidiEvent me=s.getTracks()[i].get(j);
					long tk=me.getTick();
					long t=mt.tiempo(tk);
					try{
						ShortMessage sm=(ShortMessage)me.getMessage();
						if(sm.getCommand()==ShortMessage.NOTE_ON&&sm.getData2()>0){
							c.U.imp((t*0.0441-85375)+":"+(tk*1./x));
						}
					}catch(Exception xc){
					}
					if(Tempo.getTempo(me)==null){
						me.setTick(Math.round((t/500000.)*x));
						r.getTracks()[i].add(me);
					}
				}
			}
			MidiSystem.write(r,1,new File("C:\\Desarrollo\\Audio\\Coro\\Blanca\\Bienvenida\\midi\\BIENVENIDA03F.mid"));
			c.U.imp("Fin");
		}catch(Exception e){
			c.U.imp(e);
		}
	}
	public static MetaMessage tempo(double bpm){
		return tempo(Math.round(60000000./bpm));
	}
	public static MetaMessage tempo(long v){
		byte[]datos=new byte[3];
		datos[2]=(byte)(v%256);
		v=(v-(0xFF&datos[2]));
		v=v/256;
		datos[1]=(byte)(v%256);
		v=(v-(0xFF&datos[1]));
		v=v/256;
		datos[0]=(byte)v;
		MetaMessage m2=new MetaMessage();
		try{
			m2.setMessage(81,datos,datos.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return m2;
	}
}