import java.util.*;
import javax.sound.midi.*;
import java.io.*;
public class LetrasUST{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="LetrasUST.log";
			c.U.imp("Inicio");
			Scanner u=new Scanner(new File("El-01-Alto.ust"));
			Scanner l=new Scanner(new File("El-01-Alto.lyc"));
			PrintStream ps=new PrintStream(new FileOutputStream(new File("El-01-Alto_Letra.ust")));
			while(u.hasNext()){
				String renglon=u.next();
				if(renglon.startsWith("Lyric=")&&!renglon.startsWith("Lyric=R")&&l.hasNext()){
					ps.println("Lyric="+l.next());
				}else{
					ps.println(renglon);
				}
			}
			c.U.imp("Fin");
		}catch(Exception t){
			c.U.imp(t);
		}
	}
}