import javax.sound.midi.*;
import java.io.*;
import static c.U.imp;
public class T02{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			//Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\AEDOS\\Mscore\\midi\\abre tu corazón.mid"));
			//String destino="H:\\Desarrollo\\Audio\\Coro\\AEDOS\\Mscore\\midi\\abre tu corazón-acm.mid";
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\Abschield vom Wald.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\Abschield vom Wald-acs.mid";
				//c.U.imp(Util.mapaAcordes(a,1.5)+"");
				//c.U.imp(Util.mapaAcordes(a,1)+"");
				MidiSystem.write(Util.acompanamientoAcordes(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\Abschield vom Wald.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\Abschield vom Wald-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\AEDOS\\Mscore\\midi\\abre tu corazón.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\AEDOS\\Mscore\\midi\\abre tu corazón-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1.5),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\AEDOS\\Mscore\\midi\\abre tu corazón.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\AEDOS\\Mscore\\midi\\abre tu corazón-acs.mid";
				MidiSystem.write(Util.acompanamientoAcordes(a,48,1.5),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\AEDOS\\Midi\\Olas de Piel 2024 midi.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\AEDOS\\Midi\\Olas de Piel 2024 midi-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\AEDOS\\Midi\\Olas de Piel 2024 midi.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\AEDOS\\Midi\\Olas de Piel 2024 midi-acs.mid";
				MidiSystem.write(Util.acompanamientoAcordes(a,48,1),1,new File(destino));
			}
			{
				String nombre="Amiga";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Amiga";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.acompanamientoAcordes(a,48,1),1,new File(destino));
			}
			{
				String nombre="Dedicación";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,.5),1,new File(destino));
			}
			{
				String nombre="Dedicación";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.acompanamientoAcordes(a,48,1),1,new File(destino));
			}
			{
				String nombre="Die Nachtigall";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,.5),1,new File(destino));
			}
			{
				String nombre="Die Nachtigall";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.acompanamientoAcordes(a,48,.5),1,new File(destino));
			}
			{
				String nombre="Hirtenlied_v3";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Hirtenlied_v3";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.acompanamientoAcordes(a,48,1),1,new File(destino));
			}
			{
				String nombre="Las Golondrinas";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Las Golondrinas";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.acompanamientoAcordes(a,48,1),1,new File(destino));
			}
			{
				String nombre="Neujahrslied2";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Neujahrslied2";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.acompanamientoAcordes(a,48,1),1,new File(destino));
			}
			{
				String nombre="Serenata3";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Serenata3";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.acompanamientoAcordes(a,48,1),1,new File(destino));
			}
			{
				String nombre="Vaguemos amor mío_2";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acm.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Vaguemos amor mío_2";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.acompanamientoAcordes(a,48,1),1,new File(destino));
			}
			{
				String nombre="If ye love me";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="IMSLP_Aveverumcorpus";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Jesu Meine Freude";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="JubilateDeo";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Las Golondrinas_v2";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="O Vos Omnes";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Panis1";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Panis2";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Quarti";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Santus Benedictus";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			{
				String nombre="Sicut cervus";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,2),1,new File(destino));
			}
			{
				String nombre="Vexilla Regis";
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\"+nombre+".mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\acm\\"+nombre+"-acs.mid";
				MidiSystem.write(Util.simpleAcompanamiento(a,48,1),1,new File(destino));
			}
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}
