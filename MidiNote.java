import java.io.*;
import javax.sound.midi.*;
public class MidiNote{
	public static void main(String[]args){
		try{
			String nom="Jacinto Tenor Solo";
			int c=1;
			PrintStream ps=new PrintStream(new FileOutputStream(new File(nom+".js")));
			Sequence s=MidiSystem.getSequence(new File(nom+".mid"));
			Track[]ts=s.getTracks();
			Track t=ts[c];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				try{
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(sm.getCommand()==ShortMessage.NOTE_ON&&sm.getData2()>0){
						ps.println("n.push("+sm.getData1()+");");
					}
				}catch(Exception e){
				}
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}