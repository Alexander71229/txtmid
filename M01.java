import javax.sound.midi.*;
import java.io.*;
import java.util.*;
import static c.U.imp;
public class M01{
	public static void sin(double[]d,int n,long a,long b){
		n=n-12;
		double f=WGen.gf(n,0,0);
		int ia=(int)Math.round(a*441./10000.);
		int ib=(int)Math.round(b*441./10000.);
		for(int i=ia;i<ib;i++){
			d[i]+=15000.*Math.sin(2*f*Math.PI*i/44100.);
		}
	}
	public static void main(String[]argumentos){
		c.U.ruta="log.txt";
		try{
			imp("Inicio");
			//String destino="H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Midi\\T\\02-A1-2_T";
			String destino="G:\\Java\\Txtmid\\txtmid\\CumplePiano";
			String dmid=destino+".mid";
			Sequence s=MidiSystem.getSequence(new File(dmid));
			MTempo mts=new MTempo(s);
			ArrayList<Integer>n=new ArrayList<>();
			ArrayList<Long>a=new ArrayList<>();
			ArrayList<Long>b=new ArrayList<>();
			HashMap<Integer,Long>h=new HashMap<>();
			long mx=0;
			for(int i=0;i<s.getTracks().length;i++){
				Track t=s.getTracks()[i];
				for(int j=0;j<t.size();j++){
					MidiEvent me=t.get(j);
					if(me.getMessage()instanceof ShortMessage){
						ShortMessage sm=(ShortMessage)me.getMessage();
						if(sm.getChannel()==1){
							if(sm.getCommand()==0x90&&sm.getData2()>0){
								h.put(sm.getData1(),mts.tiempo(me.getTick()));
								continue;
							}
							if((sm.getCommand()==0x90&&sm.getData2()==0)||(sm.getCommand()==0x80)){
								long x=mts.tiempo(me.getTick());
								n.add(sm.getData1());
								a.add(h.get(sm.getData1()));
								b.add(x);
								h.remove(sm.getData1());
								if(x>mx){
									mx=x;
								}
								continue;
							}
						}
					}
				}
			}
			double[]d=new double[(int)Math.ceil(mx*441./10000.)];
			for(int i=0;i<n.size();i++){
				sin(d,n.get(i),a.get(i),b.get(i));
			}
			Wave.p(d,destino+"_m01.wav");
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}