import javax.sound.midi.*;
import java.io.*;
import static c.U.imp;
public class T03{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\Vexilla Regis.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\Vexilla Regis.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\Vexilla Regisx2.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\Vexilla Regisx2.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\Las Golondrinas_v2.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\Las Golondrinas_v2.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\IMSLP_Aveverumcorpus.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\IMSLP_Aveverumcorpus.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\O Vos Omnes.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\O Vos Omnes.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\If ye love me.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\If ye love me.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\Panis1.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\Panis1.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\Panis2.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\Panis2.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\JubilateDeo.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\JubilateDeo.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\Quarti.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\Quarti.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\Sicut Cervus.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\Sicut Cervus.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\Jesu Meine Freude.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\Jesu Meine Freude.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\Santus Benedictus.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\Santus Benedictus.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\To Thee, the Holy Ghost, We now pray.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\To Thee, the Holy Ghost, We now pray.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
			{
				Sequence a=MidiSystem.getSequence(new File("H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\omr\\02\\Ave Maria_Schubert_SATB.mid"));
				String destino="H:\\Desarrollo\\Audio\\Coro\\Los Gatos\\Midi\\Pros\\02\\Ave Maria_Schubert_SATB.mid";
				MidiSystem.write(Util.to120(a),1,new File(destino));
			}
 			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}
